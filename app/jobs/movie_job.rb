class MovieJob < ApplicationJob
  queue_as :default

  def perform(message)
    
    require 'movie_controller.rb'
    
    MovieController.get_movie
 
  end
end